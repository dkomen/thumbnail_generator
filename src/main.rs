use image::{imageops, jpeg::JpegEncoder};
use rand::thread_rng;
use rand::seq::SliceRandom;
extern crate image;


mod enums;
mod arguments;
mod file;
mod structs;

use image::GenericImageView;

use std::{fs::create_dir_all, io::prelude::*};


fn main() {
    println!("Started..");
    let mut background_images = structs::BackgroundImages{Pages: vec![]};
    let mut page_images_count = 1;
    let mut total_images_count = 1;
    let mut images: Vec<structs::BackgroundImage>;
    let mut current_page = structs::Page { Images: vec![] };

    let result_arguments =  arguments::get_arguments();
    if result_arguments.is_ok() {
        let arguments = result_arguments.unwrap();
        let source_dir = arguments::get_argument_value("-i", &arguments).unwrap();
        let output_dir = arguments::get_argument_value("-o", &arguments).unwrap();
        let _ = std::fs::create_dir_all(format!("{}/large", output_dir));

        let _ = std::fs::create_dir_all(format!("{}/smaller", output_dir));

        let result_read_dir = std::fs::read_dir(&source_dir);
        let mut paths: Vec<String> = vec![];
        let mut rng = rand::thread_rng();

        if result_read_dir.is_ok() {
            for r in result_read_dir.unwrap() {
                if r.is_ok() {
                    let entry = r.unwrap();
                    if entry.path().is_file() {
                        paths.push(entry.path().to_str().unwrap().into());
                    }
                } else {
                    println!("Could not read: {}", r.unwrap_err());
                }
            }
            paths.shuffle(&mut thread_rng());
            for path in paths{
                let result_image = image::io::Reader::open(path.clone()).unwrap().with_guessed_format().unwrap().decode();
                if result_image.is_ok(){
                    let image = result_image.unwrap();
                    //let thumbnail = image.resize(750, 750, imageops::FilterType::Nearest);
                    //let encoder = JpegEncoder::new_with_quality(w, 85);
                    if image.dimensions().0 > 699 && image.dimensions().1 > 699 {
                        let new_file_name = get_random_alphanumeric_characters(10);
                        //let _ = thumbnail.save(&format!("{}/{}x{}_{}.png", output_dir, image.dimensions().0, image.dimensions().1, new_file_name));
                        let mut extension = std::path::PathBuf::from(path.clone()).extension().unwrap().to_string_lossy().to_ascii_lowercase();
                        if extension=="jpeg" {
                            extension = "jpg".into();
                        }
                        let new_file_name = format!("{}_{}x{}_{}.{}", total_images_count, image.dimensions().0, image.dimensions().1, new_file_name, extension);
                        let destination_large = format!("{}/large/{}", output_dir, new_file_name);
                        let destination_small = format!("{}/smaller/{}", output_dir, new_file_name);

                        let r = std::fs::copy(path.clone(), destination_large.clone());
                        if r.is_err() {
                            println!("Copy Error: {} - {}", path.clone(), r.unwrap_err());
                        } else {
                            // let smaller = std::fs::copy(path.clone(), destination_small.clone());
                            // if smaller.is_err() {
                            //     println!("O: {}", path.clone());
                            //     println!("L: {}", destination_large);
                            //     println!("S: {}", destination_small);
                            //     println!("Copy Error (smaller): {} - {}", path.clone(), smaller.unwrap_err());
                            // }
                        }
                        let image = structs::BackgroundImage { Path: new_file_name, Size: format!("{}x{}", image.dimensions().0, image.dimensions().1) };
                        current_page.Images.push(image);
                        if page_images_count == 75 {
                            page_images_count = 1;
                            background_images.Pages.push(current_page.clone());
                            current_page = structs::Page { Images: vec![] };
                            println!("New page: {}", total_images_count);
                        } else {
                            page_images_count+=1;
                        }
                        total_images_count+=1;
                        // if total_images_count==225{
                        //     break;
                        // }
                    } else {
                        println!("Skipping: {}x{}", image.dimensions().0, image.dimensions().1);
                    }
                } else {
                    println!("Error: {:?}", path);
                }
            }
            background_images.Pages.push(current_page.clone());

            let file_data = serde_json::to_string_pretty(&background_images).unwrap();
            file::write_to_file(&format!("{}/backgroundImages.json", output_dir), &file_data.as_bytes().to_vec(), false);
        } else {
            println!("Could not read directory: {}", source_dir);
        }

        println!();
        println!("Finished");
    }
}

pub fn get_random_characters_from(character_set: Vec<&str>, character_count: u16) -> String {
    let mut selected_characters: Vec<&str> = vec![];
    for _ in 0..character_count {
        selected_characters.push(character_set[get_a_random_number_in_range(0, (character_set.len() - 1) as u64) as usize]);
    }

    selected_characters.into_iter().collect()
}

///Get a string of random alphanumeric characters
pub fn get_random_alphanumeric_characters(character_count: u16) -> String {
    let character_set: Vec<&str> = vec! [
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
        "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
        "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1",
        "2", "3", "4", "5", "6", "7", "8", "9",
    ];
    get_random_characters_from(character_set, character_count)
}

///Get a random number within a specified number range
pub fn get_a_random_number_in_range(start_value: u64, end_value: u64) -> u64 {
    use rand::Rng;
    let mut rng = rand::thread_rng();

    rng.gen_range(start_value, end_value)
}
