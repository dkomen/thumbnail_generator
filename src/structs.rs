#[derive(serde::Serialize, serde::Deserialize, Debug, Clone)]
pub struct BackgroundImage {
    pub Path: String,
    pub Size: String
}

#[derive(serde::Serialize, serde::Deserialize, Debug, Clone)]
pub struct Page {
  pub Images: Vec<BackgroundImage>
}

#[derive(serde::Serialize, serde::Deserialize, Debug, Clone)]
pub struct BackgroundImages {
    pub Pages: Vec<Page>
}