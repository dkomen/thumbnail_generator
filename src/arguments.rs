//! A command line arguments processor.

use std::env;

/// Represents a specific commmand-line argument and its value/
///
/// Only used in this declarting module
#[derive(Clone)]
pub struct Argument{
    pub name: String,
    pub value: String
}

/// Get arguments from command-line
pub fn get_arguments() -> Result<Vec<Argument>, ()> {            
    let args_array: Vec<String> = env::args().collect::<Vec<String>>();
    let arguments: Result<Vec<Argument>, ()> = get_settings(args_array);

    arguments
}
//-f f.txt -p -d
fn check_arguments(args_array: &Vec<String>) -> bool {
    let mut next_must_be_argument = true;
    for i in 2..args_array.len() {
        if args_array[i].starts_with("-") {
            next_must_be_argument = false;
        } else {
            next_must_be_argument = !next_must_be_argument;
        }
    }

    true
}

/// If an argument exists on the command line then add it and its value to Argument Vec to return
fn get_settings(args_array: Vec<String>) -> Result<Vec<Argument>, ()> {    
    let mut arguments_to_return: Vec<Argument> = vec!();

    if args_array.len() == 1 {    
        println!("{}", get_help_screen_text());
        return Err(())
    }

    let mut index_i_exists: bool = false;
    let mut index_o_exists: bool = false;

    if !check_arguments(&args_array) { return Err(()) }

    let index_h = get_argument("-h", &args_array); // Help screen
    let index_h_help = get_argument("--help", &args_array); // Help screen
    let index_i = get_argument("-i", &args_array); // Input directory
    let index_o = get_argument("-o", &args_array); // Output directory

    if index_h.is_some() || index_h_help.is_some() {
        println!("{}", get_help_screen_text());
        return Err(());
    } else {
        if index_i.is_some() {
            index_i_exists = true;
            arguments_to_return.push(index_i.unwrap());
        };
        if index_o.is_some() {
            index_o_exists = true;
            arguments_to_return.push(index_o.unwrap());
        };

    }

    if index_h.is_some() {
        println!("{}", get_help_screen_text());
        return Err(());
    } else {
        if index_i_exists {
            let argument_value = get_argument_value("-i", &arguments_to_return);       
            if argument_value.is_some() {
                let mut value = argument_value.unwrap();                
                if value.len() == 0 {
                    println!("The value for argument '-i' was not supplied (-h for help).");
                    return Err(());
                } else if value.starts_with(".\\") || value.starts_with("./") {
                    value.replace_range(..2, "");
                    arguments_to_return = set_argument_value("-f", &value, &mut arguments_to_return);
                    println!("Name changed to {}.", value);
                } else if value.starts_with(".") {
                    println!("The value for argument '-i' may not start with a '.' character (-h for help).");
                    return Err(());
                }
            } else {
                println!("Argument error for '-i' (-h for help).");
                return Err(());  
            }
        };

        if index_o_exists {
            let argument_value = get_argument_value("-o", &arguments_to_return);       
            if argument_value.is_some() {
                let mut value = argument_value.unwrap();                
                if value.len() == 0 {
                    println!("The value for argument '-o' was not supplied (-h for help).");
                    return Err(());
                } else if value.starts_with(".\\") || value.starts_with("./") {
                    value.replace_range(..2, "");
                    arguments_to_return = set_argument_value("-o", &value, &mut arguments_to_return);
                    println!("Name changed to {}.", value);
                } else if value.starts_with(".") {
                    println!("The value for argument '-o' may not start with a '.' character (-h for help).");
                    return Err(());
                }
            } else {
                println!("Argument error for '-i' (-h for help).");
                return Err(());  
            }
        };
    }

    Ok(arguments_to_return)
}

/// Get the index of the argument we want in the command line arguments array 
fn get_argument(arg_id: &str, args_array: &Vec<String>) -> Option<Argument> {
    let arg_index = args_array.iter().position(|arg| arg.starts_with(arg_id)).unwrap_or(0);    
    let mut argument = Argument {
        name: arg_id.to_owned(),
        value: String::new()
    };

    if arg_index >= 1 {
        let result = get_arg_value(arg_index, args_array);
        if result.is_ok() {
            argument.value=result.unwrap();
            return Some(argument);
        }
    }

    None
}

/// Get a specific argument switches' value
fn get_arg_value(index: usize, arg_array: &Vec<String>) -> Result<String,()> {    
    if arg_array.len() > index + 1 {
        let next_value = arg_array[index + 1].to_owned();
        if next_value.starts_with("-") {
            Ok(String::new())
        } else {            
            Ok(next_value)
        }
    } else {
        return Ok(String::new());
    }
}

/// Set an arguments value
pub fn set_argument_value(argument_name: &str, new_value: &str, arguments: &mut Vec<Argument>) -> Vec<Argument> {
    arguments.retain(|arg| arg.name != String::from(argument_name));

    arguments.push(Argument{name: String::from(argument_name), value: String::from(new_value)});    

    arguments.clone()
}

/// Get the value of an argument if it was supplied on the command line
pub fn get_argument_value(argument_name: &str, arguments: &Vec<Argument>) -> Option<String> {
    for argument in arguments {
        if argument.name == argument_name {
            return Some(argument.value.to_owned());
        }
    };
    None
}

/// Builds a string containing the Help screen text.
///
/// Displayed when the user runs the client with no arguments, makes a mistake with arguments entered or explicitly invoking Help.
fn get_help_screen_text() -> String {
    let mut help_screen_text = format!("");
    help_screen_text = help_screen_text + "     ____                 _____         _    ";
    help_screen_text = help_screen_text + "\n    / ___|_ __ _   _  ___|__  /___ _ __| | __";
    help_screen_text = help_screen_text + "\n   | |   | '__| | | |/ _ \\ / // _ \\ '__| |/ /";
    help_screen_text = help_screen_text + "\n   | |___| |  | |_| | (_) / /|  __/ |  |   < ";
    help_screen_text = help_screen_text + "\n    \\____|_|   \\__, |\\___/____\\___|_|  |_|\\_\\";
    help_screen_text = help_screen_text + "\n               |___/";
    help_screen_text = help_screen_text + "\n\nTransfer files to anywhere in the world using strong end-to-end encryption.";
    help_screen_text = help_screen_text + "\nWritten by Dean Komen 2020";
    help_screen_text = help_screen_text + "\n\nLICENSE: Dimension 15 General Public License v1 <https://www.dimension15.co.za/>";
    help_screen_text = help_screen_text + "\nSource code: https://www.gitlab.com/dkomen/cryozerk";
    help_screen_text = help_screen_text + "\nWebsite: https://www.dimension15.co.za/products/cryozerk";
    help_screen_text = help_screen_text + "\n\nCommands:";
    help_screen_text = help_screen_text + "\n    register: Register the client for secure server communications";
    help_screen_text = help_screen_text + "\n        keys: Show a list of all the keys available for use in the configuration file";
    help_screen_text = help_screen_text + "\n        send: Send a file to the server [-f, -p, -d, -v, -x, -k, -l]";
    help_screen_text = help_screen_text + "\n         get: Retrieve a previously sent file from the server [-c, -p, -v, -k, --display]";
    help_screen_text = help_screen_text + "\n        list: Retrieve a list of all files uploaded for your access key [-l. -k]";
    help_screen_text = help_screen_text + "\n      delete: Delete a specific file with your access key and unique code from the server [-c, -p. -k]";
    //help_screen_text = help_screen_text + "\n     upgrade: Retrieve the lastest client from the server";
    help_screen_text = help_screen_text + "\n\nArguments:";
    help_screen_text = help_screen_text + "\nArgument value supplied must be inside quotes if they contain special characters - '3eCRet!file@.txt'";
    help_screen_text = help_screen_text + "\n        -h: 'Help': Display this help screen";
    help_screen_text = help_screen_text + "\n        -f: 'Input file': The file you wish to upload/send";

    help_screen_text = help_screen_text + "\n";

    help_screen_text = help_screen_text + "\n";

    help_screen_text
}

/// An easter egg
fn get_smile_screen_text() -> String {
    let mut help_screen_text = format!("\n");
    help_screen_text = help_screen_text + "\n    (ʘ‿ʘ)";
    help_screen_text = help_screen_text + "\n";
    help_screen_text = help_screen_text + "\n\n\n";
    help_screen_text
}