use std::fs::File;
use std::io::prelude::*;

/// Write a vector to a file
pub fn write_to_file(path: &str, file_data: &Vec<u8>, append_to_file: bool) -> bool {
    if !append_to_file && file_exists(&path) {
        let _ = delete_file(&path);
    }
    let file_handle = std::fs::OpenOptions::new().create(true).write(true).append(append_to_file).open(path);
    let mut file_handle = match file_handle {
        Ok(file) => file,
        Err(error) => {
            println!("Error creating the file '{}': Error:{:?}", path, error);
            return false;
        }
    };

    let results = file_handle.write(&file_data);
    let _ = match results {
        Ok(results) => results,
        Err(error) => {
            println!("Error writing file '{}': {:?}", path, error);
            return false;
        }
    };

    true
}

/// Delete a file from disk
pub fn delete_file(file_path: &str) -> bool {
    if file_exists(&file_path) {
        if std::fs::remove_file(&file_path).is_ok() {
            true
        } else {
            println!("Could not delete file: {}", file_path);

            false
        }
    } else {
        // File doesn't exist, so a delete success is reported
        true
    }
}

/// Does a file exist?
pub fn file_exists(path: &str) -> bool {
    let path_to_file_buff = std::path::PathBuf::from(path);
    let path_to_file = path_to_file_buff.to_str().unwrap();

    std::path::Path::new(path_to_file).exists()
}
