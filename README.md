# Thumbnail Generator
Used to generate thumbnails from larger images and then copying the thumbnails and originals into new directories with a json (includes image size in pixels) file to map them to each other.
Note!: Thumbnails are not currently generated (due to poor quality of thumbnails, use an external program instead) but the json file and the larger images are processed

~~~
./thumbnail_generator -i /home/dk/Pictures/D15Walls -o /home/dk/Pictures/Out
~~~

Use this one!